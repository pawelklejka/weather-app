import React from "react";
import Form from "./components/Form";
import Titles from "./components/Titles";
import Weather from "./components/Weather";


const API_KEY = "9b207bc9d88912ee69f2fc15946479e5";


class App extends React.Component{

  state = {
    temperature: undefined,
    city: undefined,
    country: undefined,
    humidity: undefined,
    description: undefined,
    error: undefined
  }
  getWeather = async (e) => {
    const city = e.target.elements.city.value;
    const country = e.target.elements.country.value;
    e.preventDefault();
    const api_call = await fetch(`
    http://api.openweathermap.org/data/2.5/weather?q=${city},${country}&appid=${API_KEY}&units=metric`);

    const data = await api_call.json();

    if(city && country){
      console.log(data);
  
      this.setState({
        temperature: data.main.temp,
        city: data.name,
        country: data.sys.country,
        humidity: data.main.humidity,
        description: data.weather[0].description,
        error: ""
      })
    }else{
      console.log(data);
  
      this.setState({
        temperature: undefined,
        city: undefined,
        country: undefined,
        humidity: undefined,
        description: undefined,
        error: "Enter the city and country"
      })
    }

  }
  render(){
    return(
      <div>
          <div className="wrapper">
            <div className="main">
              <div className="container">
                <div className="row">
                  <div className="col-lg-5 title-container">
                    <Titles />
                  </div>
                  <div className="col-lg-7 form-container">
                    <Form onClick={this.getWeather}/>
                    <Weather 
                      temperature={this.state.temperature}
                      humidity={this.state.humidity}
                      city={this.state.city}
                      country={this.state.country}
                      description={this.state.description}
                      error={this.state.error}/>
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div>  
      
    );
  }
};


export default App;