import React from "react";

const Weather = props =>(
    
        <div className="weather__info">
        { props.city && <p>city: {props.city}</p>}
        { props.country && <p>country: { props.country}</p>}
        { props.temperature && <p>temperature: { props.temperature}</p>}
        { props.humidity && <p>humidity: { props.humidity}</p>}
        { props.description &&<p>description: { props.description}</p>}
        { props.error && <p>{ props.error}</p>}
        </div>
    );
export default Weather;